namespace EmployeeWebApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EmployeeWebApp.Models.EmployeeWebAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "EmployeeWebApp.Models.EmployeeWebAppContext";
        }

        protected override void Seed(EmployeeWebApp.Models.EmployeeWebAppContext context)
        {
            /* d9e029d5 - c9fc - 4bfe - 8f47 - a58ea40602b9
               e2ad67ce - 9e39 - 41cf - 989b - 9e02ec4e7a05 */
         
                context.Contacts.AddOrUpdate(
                  p => p.Id,
                  new Models.Contact { Id = 1, Birthday = new DateTime(1920, 01, 20), City = "Poland"
                                      ,Email = "test1@gmail.com", FirstName = "Dawid", LastName = "Syczewski", PhonePrimary = "111-333-222"
                                      ,PhoneSecondray = "222-111-353", PostCode = "15-324", Region = "Podlasie", StreetAddress1 = "Boh. Monte Cassino"
                                      ,StreetAddress2 = "Przydworcowe", UserId = new Guid("d9e029d5-c9fc-4bfe-8f47-a58ea40602b9")
                  }
                  ,new Models.Contact { Id = 2, Birthday = new DateTime(1920, 01, 20), City = "Poland"
                                      ,Email = "test1@gmail.com", FirstName = "Alina", LastName = "Syczewska", PhonePrimary = "111-333-222"
                                      ,PhoneSecondray = "222-111-353", PostCode = "15-324", Region = "Podlasie", StreetAddress1 = "Boh. Monte Cassino"
                                      ,StreetAddress2 = "Przydworcowe", UserId = new Guid("e2ad67ce-9e39-41cf-989b-9e02ec4e7a05")
                  }
                  ,new Models.Contact { Id = 3, Birthday = new DateTime(1920, 01, 20), City = "Poland"
                                      ,Email = "test1@gmail.com", FirstName = "David", LastName = "Syczewski", PhonePrimary = "111-333-222"
                                      ,PhoneSecondray = "222-111-353", PostCode = "15-324", Region = "Podlasie", StreetAddress1 = "Boh. Monte Cassino"
                                      ,StreetAddress2 = "Przydworcowe", UserId = new Guid("e2ad67ce-9e39-41cf-989b-9e02ec4e7a05")
                  }
                  
                );

        }
    }
}
